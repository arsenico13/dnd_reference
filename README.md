# DnD Reference

Repo dedicato a raccogliere le informazioni su regole, campagne, istruzioni o altro relativo a DnD.


<!-- TOC -->

- [Campagna West Marches](./west_marches/README.md)
- [Interazione tra Dungeon Master e Giocatore](./giocare/README.md)
- [Mappe](./mappe/README.md)
- [Preparare una campagna](./giocare/campagna.md)

<!-- /TOC -->

---
