# Il gioco


## Interazione tra Dungeon Master e Giocatore


### Descrivere il proprio stile di Dungeon Master

Vedi [SlyFlourish](https://slyflourish.com/describe_your_gm_style.html) e anche [The Same Page](https://bankuei.wordpress.com/2010/03/27/the-same-page-tool/)

- "Most gamers assume the one or two ways they’ve played is how you play ALL games." - cit.


### Descrivere le **intenzioni al posto della azioni**

- I giocatori, soprattutto agli inizi, è meglio che descrivano quello che vogliono fare al posto di pensare in base alle "azioni disponibili". Nel primo caso il DM può aggiungere informazioni, richiedere dei test specifici, dare bonus/malus. Nel secondo caso il giocatore si auto-limiterà in partenza rischiando di fare cose meno divertenti, interessanti o epiche.
- Qui un concetto simile ma dal punto di vista del design: [link](https://montecook.substack.com/p/intentions-before-actions)


---
