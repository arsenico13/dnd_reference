# Preparazione di una campagna

## Guida/presentazione della campagna in una singola pagina

Seguendo l'idea di SlyFlourish, [vedi qui](https://slyflourish.com/one_page_campaign_guide.html), è consigliabile scrivere una guida/presentazione della campagna che si intende giocare e fare in modo che tutto resti in un'unica pagina. Cerchiamo di restare concisi, diretti, accattivamti.

Nella pagina linkata ci sono anche degli esempi per farsi un'idea.

ps: Vedi anche [Limitare i libri sorgenti](#limitare-i-libri-sorgenti).


## Limitare i libri sorgenti

Meglio non lasciare che i giocatori possano scegliere da **qualsiasi libro mai pubblicato** per D&D. Si creerebbero combinazioni strane, molto probabilmente rotto e OP, si rischierebbe di non rimanere il "tema" con la campagna scelta e si potrebbe fare confusione.

Ricordiamoci di specificare subito ai giocatori quali sono le risorse che possono utilizzare per creare i propri personaggi.

[A questo link](https://slyflourish.com/limiting_sources_in_dnd_beyond.html) c'è questo discorso spiegato meglio.
