# I princìpi del gioco

Appunti

> I princìpi del gioco variano in base a tantissimi fattori. Uno dei più importanti è il tono ed il contesto che il Dungeon Master vuole dare ad una particolare sessione/campagna.


- Il gioco __non__ dice ai giocatori cosa dovrebbero pensare o sentire, quali missioni accettare o dove dovrebbero andare. **Gli avventurieri predono le proprie decisioni e se ne assumono le conseguenze**. Il "bene puro" o il "male puro" è raro da trovare, saranno i giocatori a decidere cosa considerare giusto e sbagliato e da quale parte schierarsi.
- La **morte** di un personaggio fa parte della storia. Nessun avventuriero è invincibile/immortale.
- Combattimenti: qualsiasi giocatore dovrebbe sempre tenere a mente che è possibile (anche se non sempre) tentare di scappare da un combattimento e non è sempre necessario combattere fino all'ultimo sangue (proprio o nemico).

---
