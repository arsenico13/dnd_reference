# Mappe

## Info sulle mappe di gioco e la loro funzione

Al giorno d'oggi le mappe vengono considerate come schemi precisi, manuali di istruzioni che mostrano le informazioni nella maniera più accurata possibile. Abbiamo tantissimi tipi di mappe, perlopiù di carattere scientifico.

Fin dall'antichità, molte delle convenzioni utilizzate per la creazione di mappe non sono nient'altro che questo: convenzioni. Non sono delle effettive applicazioni di cose misurabili.

In modo da poter creare delle mappe leggibili e utili dobbiamo scegliere le nostre priorità e il nostro obiettivo. Dobbiamo mostrare alcune cose e ometterne altre. Questo significa creare e seguire un piano, un progetto. Ogni progetto è soggettivo, influenzato dalla cultura e dalla politica di chi lo crea.
Da questo punto vi vista, possiamo vedere le mappe come un riflesso della conoscenza e dell'interpretazione soggettiva di chi la crea.
Così facendo si arriva a pensare che i posti inventati e le mappe di fantasia non sia poi così differenti dai corrispettivi reali.


> TODO: completare il discorso


- source:
  - https://www.youtube.com/watch?v=8O4dWMXqNrE







---