# West Marches

> !!! Documento per dungeon master !!!

<!-- TOC -->

- [Introduzione](#introduzione)
- [Dettagli](#dettagli)
- [Approfondimenti](#approfondimenti)
- [Link Utili](#link-utili)
- [Risorse](#risorse)

<!-- /TOC -->


## Introduzione

Una campagna in stile "West Marches" è una particolare tipologia di campagna che permette la partecipazione di un grande numero di giocatori/personaggi che si avventurano insieme nello stesso mondo di gioco.


> Al di là dei confini della società umana, la natura selvaggia crea un canto di sirena che pochi riescono a sentire tra la fatica della vita quotidiana. La terra al di fuori della civiltà brulica di altre bestie fantastiche, creature ripugnanti, folletti assetati di sangue e draghi malvagi. Antiche rovine disseminano il paesaggio, offrendo tesori inimmaginabili o terrori per spaventare i bambini prima di dormire.
> 
> Dove andranno i giocatori? Quali voci perseguiranno? I cittadini e altri avventurieri li terranno volentieri informati sugli avvenimenti locali, ma le scelte su cosa fare sono nelle mani dei giocatori. Possono visitare un punto di riferimento noto, cercare l'accampamento di cacciatori o semplicemente esplorare, lasciando che l'avventura trovi loro.
> 
> Parte di ciò che si può trovare è noto, altro sarà semplicemente voci, ma la maggior parte sarà completamente sconosciuta. Scegli tu dove andare e cosa fare. Ci saranno una manciata di scelte ovvie, ma non è assolutamente necessario prenderle.


È una "hexcrawl"/sandbox costruita su misura.  In una campagna di questa tipologia, il mondo di gioco è concepito come un vasto territorio selvaggio e inesplorato in cui si trovano numerosi **punti di interesse** (città, villaggi, dungeon, caverne, ecc...). La particolarità di questa modalità di campagna sta nel fatto che _non c'è un "filo conduttore" o una trama principale_ che guida i personaggi attraverso l'avventura. Al contrario, i giocatori sono liberi di scegliere quale luogo esplorare e quali avventure intraprendere (`player driven`).

Il DM di una campagna "West Marches" si occupa di creare e descrivere il mondo di gioco e tutti i punti di interesse presenti ma spetta ai giocatori decidere dove andare e cosa fare. Inoltre, il DM non guida la trama principale, ma si limita a reagire alle azioni dei giocatori e a far evolvere il mondo di gioco in base alle loro scelte.

Questa tipologia di campagna è particolarmente adatta per gruppi numerosi di giocatori in quanto **non c'è un gruppo fisso di avventurieri**. Sono gli stessi giocatori che si mettono d'accordo su dove andare, quando e con chi. Questo permette a tutti i partecipanti di esplorare il mondo di gioco come si vuole, scegliere quali zone esplorare in base ai propri gusti e di interagire con molti altri personaggi. Inoltre, il fatto che non ci sia una trama principale predefinita o un gruppo fisso fa sì che ogni sessione di gioco sia unica e imprevedibile, offrendo una grande varietà di situazioni e avventure.

Il DM fornisce ai giocatori alcune informazioni iniziali sulle diverse località e suggerire alcune possibili avventure, ma spetta ai giocatori decidere quale percorso intraprendere. Inoltre, il DM può far evolvere il mondo di gioco in base alle scelte dei giocatori, creando nuove avventure e situazioni in base a quello che accade durante le sessioni.


_PS: Il nome "West Marches" deriva dal creatore di questa tipologia, il game designer Ben Robbins, nel 2007._

### Breve descrizione per i giocatori

Una campagna in stile "West Marches" è una campagna "sandbox", senza mappa disponibile ai giocatori.
I giocatori partono da una base di partenza (una città) all'inizio di ogni missione e tracciano il loro percorso attraverso la natura selvaggia in ogni avventura che affrontano. Alla fine della sessione i giocatori tornano alla base, lontano dalla natura selvaggia, dopo aver completato una missione.
Questi sono gli step di base. Si parte dalla base; ci si avventura per una missione; si completa la missione; si ritorno alla base di partenza.

Non c'è una schedulazione precisa delle sessioni, ci si organizza di volta in volta.
Non c'è un gruppo di gioco fisso per ogni sessione, ci si organizza di volta in volta.

Questo permetti di avere un numero illimitato di giocatori che partecipano nella stessa ambientazione contemporaneamente.

Invece che fare delle one-shot completamente disconnesse tra di loro, ogni avventura è nello stesso mondo e le azioni non vengono "perse" al termine della sessione. C'è una continuità di quello che viene fatto e scoperto.

Quindi: "west marches" è un setting comune a tante sessioni one-shot dove tanti gruppi di gioco diversi condividono lo stesso mondo e le conseguenze delle proprie azioni.
Ogni sessione si aspetta che i giocatori si avventurino nella natura selvaggia, trovare o essere trovati da avventura per poi tornare a casa alla fine della sessione.


## Perché giocare una "west marches"?

Prima di imbarcarsi di una campagna di questo tipo ci sono due cose da sapere.

La prima è che le "West marches" richiedono molto lavoro! Sia per il DM che per i giocatori. Viene richiesto più impegno rispetto ad una campagna normale. La differenza è che questo impegno può essere dilazionato su un periodo di tempo maggiore perché non è richiesta una cadenza di sessioni costante.

La seconda è che le "West marches" escludono un determinato tipo di gioco. Campagne con intensi intrighi politici per esempio soffrono con questa tipologia di gioco. Al contrario ci sarà infinita quantità di esplorazione, viaggio, gestione delle risorse.


### Maggiore flessibilità

La "player pool" dalla quale vengono "pescati" i giocatori per formare il gruppo per una sessione permette tantissima flessibilità.
Significa che non tutti devono giocare tutte le volte.

Cancellare una sessione perché qualche giocatore non può esserci? Non succederà più.
Sensi di colpa perché un giocatore può giocare solo qualche volta? Spariti.


### Narrazione

Le storie che puoi raccontare nelle "West Marches" non hanno eguali. Mentre giocate, l'impatto delle vostre azioni si farà sentire in lungo e in largo. Sicuramente influenzeranno gli NPC e le fazioni, ma si diffonderanno anche agli altri giocatori. C'è qualcosa di veramente magico nell'inciampare sul luogo di una battaglia, cadaveri e segni di bruciature sparsi in giro, solo per scoprire che questo è stato fatto dai tuoi alleati piuttosto che da tuoi nemici!
La narrazione delle "West Marches" è intrinsecamente legata alle meccaniche del gioco: dove andate e cosa fate come giocatori è la storia. Il GM non ha una trama prestabilita: il tuo destino è nelle tue mani. Una volta afferrata la libertà che deriva dal determinare interamente la propria storia, non c'è nient'altro di simile.


---


## Approfondimenti

> - [Concetti base](./concetti_base.md) 


Una serie di link utili per approfondire l'argomento su questo stile di campagna:

- Il post del creatore di "West Marches": <https://arsludi.lamemage.com/index.php/78/grand-experiments-west-marches/>
- Video:
  - Ottima presentazione generale dello stile di gioco "west marches" da Matt Colville: "The West Marches | Running the Game": <https://www.youtube.com/watch?v=oGAC-gBoX9k> 
  - "Running A D&D Game for UNLIMITED Players | West Marches Campaign | DM Academy": <https://www.youtube.com/watch?v=Yx_nDTLL7iw&t=114s>
- HEX/Esagoni:
  - In Praise of the 6 Mile Hex: <http://steamtunnel.blogspot.com/2009/12/in-praise-of-6-mile-hex.html>


---


## Link utili

- Generatori casuali che vi segnalo così su due piedi:
  - Il famosissimo e rinomato **donjon**:
    - <https://donjon.bin.sh/5e/>  generare roba specifica per la 5e
    - <https://donjon.bin.sh/fantasy/> generare roba generica fantasy
  - Il meraviglioso **slyflourish**:
    - <https://slyflourish.com/random_name_generator.html> nomi
    - <https://slyflourish.com/ancient_monuments.html> monumenti
    - <https://slyflourish.com/relics.html> reliquie
    - <https://slyflourish.com/random_trap_generator.html> trappole
  - Generatore di **scontri** casuali: <https://koboldplus.club/#/encounter-manager>
  - Generatore di un sacco di roba mista: <https://lastgaspgrimoire.com/generators/the-seventh-order-of-the-random-generator/>
- Gestione dei **combattimenti**:
  - <https://koboldplus.club/#/encounter-manager>
- Un po’ di tutto quello che c’è open per DnD 5e, ricercabile, filtrabile, gratuito (contiene anche materiale di terze parti, non sono WotC): <https://open5e.com/>
- **Dungeon Mastering**:
  - <https://thealexandrian.net/> - ottimo materiale su dm/rpg
- **Hexes**:
  - <http://welshpiper.com/hex-based-campaign-design-part-1/> e <https://welshpiper.com/hex-based-campaign-design-part-2/>
  - <https://welshpiper.com/packages/hex-templates-5-mile-scale/>
- **West Marches**: <https://www.reddit.com/r/itmejp/comments/2idf33/west_marches_resources/>


---

## Risorse

- [dndspeak.com - d100 lists](https://www.dndspeak.com/)
- "Tome of Adventure design", by Frog God Games



---
