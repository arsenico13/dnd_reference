# Concetti Base

<!-- TOC -->

- [Conoscere il gioco](#conoscere-il-gioco)
- [Il gruppo](#il-gruppo)
  - [La player pool](#la-player-pool) 
  - [La lista di personaggi](#la-lista-di-personaggi)
  - [La squadra per le missioni](#la-squadra-per-le-missioni)
- [Panoramica degli elementi di base](#panoramica-degli-elementi-di-base)

<!-- /TOC -->



## Conoscere il gioco

> TODO


### Tipologia di gioco: Campagna Sandbox

In una campagna "sandbox" i giocatori sono liberi di girare, scoprire e interagire con il mondo che gli viene presentato. Non c'è nessuna "Storia" con la S maiuscola nella quale addentrarsi perché ci sono storie dappertutto, in ogni direzione e luogo.
I giocatori hanno il potere di influenzare il mondo: le loro azioni e le loro decisioni modificano l'ambiente e hanno conseguenze.

NB: Sandbox in un videogame **non è** la stessa cosa di sandbox in un gioco di ruolo.



## Il gruppo

> TODO


### La player pool

> TODO


### La lista di personaggi

> TODO


### La squadra per le missioni

"Mission parties"

> TODO

## Panoramica degli elementi di base

> TODO


---

