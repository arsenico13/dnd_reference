# Consigli e tecniche aggiuntive per DM


## Come collegare i giocatori alla città

- Link: https://www.youtube.com/watch?v=w51u7qfHn44

1. Coinvolgere i giocatori nella creazione del mondo
   1. Ad ogni giocatore si può chiedere di inventarsi un negozio, un'attività, un'organizzazione o un punto di interesse che sia presente nella città e con il quale vorrebbero poter interagire. Qualcosa che possa essere utile o confortevole per il loro personaggio.
2. Coinvolgere i giocatori con le gilde locali
   1. Gilda dei ladri, di guerrieri, dei mercanti, dell'ordine sacro, dei muratori... qualsiasi cosa possa essere utile o interessante.
   2. Chi fa parte di una gilda potrebbe avere dei benefici in gioco, magari dei bonus o degli aiuti in determinate situazioni.
      1. Se si rompono i rapporti con la gilda per qualsiasi motivo (litigi, divergenze, si smette di pagare l'affiliazione, ...) si perdono ovviamente i benefici
3. Costruire relazioni con dei PNG
   1. Relazioni familiari, relazioni amorose, grandi amicizie, compagni di bevute, ...
   2. Questi PNG non devono creare problemi ma
   3. Questi PNG possono essere fonte di rumor, gossip o quest
4. Alloggio
   1. Fare in modo che i giocatori (i personaggi) si interessino nel comprare un qualche tipo di alloggio nella città.
   2. Possono affittare una casa o magari comprarla. On un locale.
   3. Magari un qualche PNG che incontrano nelle prime sessioni ha una casa che vuole vendere... o un appartamento che vuole affittare.
   4. Non deve essere per forza qualcosa di nuovo, anzi. Può essere qualcosa che sia da sistemare e da personalizzare in modo che abbiamo anche uno scopo per poter spendere le fortune che faranno con le proprie avventure.




---